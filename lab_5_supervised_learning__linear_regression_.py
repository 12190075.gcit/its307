# -*- coding: utf-8 -*-
"""Lab 5 - Supervised Learning (Linear Regression).ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1wySwP8suJrwcHIB8MSC42W6AqG9Abxb3

# ITS307 Data Analytics                                                   : Spring Semester 2022
# Practical 5
# Supervised Learning: Linear Regression

![image-3.png](attachment:image-3.png)

# Table of Contents 
<ol start="0">
<li> Learning Objectives </li>
<li> Importing Libraries </li>
<li> Loading and Cleaning with Pandas</li>
<li> EDA  </li>
<li> Training Model</li>
<li> Predicting</li>
</ol>

## Learning Objectives

This dataset contains information collected by the U.S Census Service concerning housing in the area of Boston Mass. It was obtained from the StatLib archive (http://lib.stat.cmu.edu/datasets/boston), and has been used extensively throughout the literature to benchmark algorithms. However, these comparisons were primarily done outside of Delve and are thus somewhat suspect. The dataset is small in size with only 506 cases.

The data was originally published by Harrison, D. and Rubinfeld, D.L. `Hedonic prices and the demand for clean air', J. Environ. Economics & Management, vol.5, 81-102, 1978.

## Variables
There are 14 attributes in each case of the dataset. They are:
1. CRIM - per capita crime rate by town
2. ZN - proportion of residential land zoned for lots over 25,000 sq.ft.
3. INDUS - proportion of non-retail business acres per town.
4. CHAS - Charles River dummy variable (1 if tract bounds river; 0 otherwise)
5. NOX - nitric oxides concentration (parts per 10 million)
6. RM - average number of rooms per dwelling
7. AGE - proportion of owner-occupied units built prior to 1940
8. DIS - weighted distances to five Boston employment centres
9. RAD - index of accessibility to radial highways
10. TAX - full-value property-tax rate per $10,000

11. PTRATIO - pupil-teacher ratio by town
12. B - 1000(Bk - 0.63)^2 where Bk is the proportion of blacks by town
13. LSTAT - % lower status of the population
14. MEDV - Median value of owner-occupied homes in $1000's (**TARGET**)

By the end of the lab, you should be able to :
- Load and systematically address missing values, ancoded as NaN values in our data set, for example, by removing observations associated with these values.

- Parse columns in the dataframe to create new dataframe columns.

- Create and interpret visualizations to explore the data set and relationships between variables
- Create a simple Linear Model to predict Housing prize given features values

## Importing Libraries
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_boston

"""# Part 1: Simple Linear Regression

## Loading and Cleaning data with pandas
"""

data = load_boston()
for keys in data:
    print(keys)

print(data.DESCR)

data

df = pd.DataFrame(data.data,columns = data.feature_names)
df.head()

df.shape

df['MEDV'] = data.target
df.head()

"""## EDA

Choose any features to explore relationship with target variables.
"""

import seaborn as sns
sns.set(rc={'figure.figsize':(15.7,8.27)})
sns.scatterplot(data = df,x = 'RM',y = 'MEDV')
plt.xlabel("Number of rooms")
plt.ylabel("Median Value in $1000's")
plt.show()

"""## Training Model"""

#create instance of LinearRegression
model = LinearRegression()
model

model.fit(np.array(df.RM).reshape(-1,1),df.MEDV)
model

model.coef_

model.intercept_

"""## Prediction"""

x = np.array([6.25]).reshape(-1,1)
model.predict(x)

"""## Check MSE"""

x = np.array(df.RM).reshape(-1,1)

from sklearn.metrics import mean_squared_error
y_pred = model.predict(x)

MSE = mean_squared_error(df.MEDV,y_pred)

MSE

sns.set(rc={'figure.figsize':(15.7,8.27)})
sns.scatterplot(data = df,x = 'RM',y = 'MEDV')
plt.plot(x, y_pred,color='red')
plt.xlabel("Number of rooms")
plt.ylabel("Median Value in $1000's")
plt.show()

"""# Part 2: Multiple Linear Regression(TODO 5)

## EDA
"""

#Taking 5 attributes for multiple regression and displaying the top 5 values
arg = ['RM','INDUS','LSTAT','DIS','RAD']
x = df[['RM','INDUS','LSTAT','DIS','RAD']]
x.head()

"""## Training Multiple Regression Model"""

#Linear Regression instance
linear_reg = LinearRegression()

#Calculating the optimal values of the weights/parameters(i.e: 'x' and 'df.MEDV')
linear_reg.fit(x,df.MEDV)

LinearRegression()

#Getting the coefficient of each arguments
coe_reg = linear_reg.coef_
coe_reg

#produces an intercept 
int_reg = linear_reg.intercept_
int_reg

"""## Predicting Value"""

y = np.array([6.575,2.31,4.98,4.0900,1.0])
mp = y.reshape(-1,len(arg))
linear_reg.predict(mp)